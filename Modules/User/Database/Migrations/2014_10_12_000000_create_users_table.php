<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();

            $table->string('md5')->nullable();

            $table->string('first_name')->nullable();

            $table->string('last_name')->nullable();

            $table->string('email')->unique()->nullable();

            $table->boolean('newsletter_subscribe')->default(1);

            $table->string('avatar')->default('avatar.png');

            $table->string('mobile')->unique()->nullable();

            $table->string('phone')->unique()->nullable();

            $table->string('verification_code')->unique()->nullable();

            $table->timestamp('mobile_verified_at')->nullable();

            $table->timestamp('email_verified_at')->nullable();

            $table->boolean('status')->default(1);

            $table->string('password')->nullable();

            $table->string('national_code')->nullable();

            $table->date('birth')->nullable();

            $table->timestamp('last_login_at')->nullable();

            $table->string('last_login_ip')->nullable();

            $table->rememberToken();

            $table->softDeletes();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
};
